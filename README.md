Java Webcrawler for ranking most used Javascript libraries

### What I could have improved

- I would have like to implement some integration testing, using mock server (https://www.mock-server.com/) instead of calling to the Google API.
Mocking external dependencies is an integral part of this type of test.

- I had a clean architecture by Bob Martin in mind, but when trying to structure the use-cases I saw that the project was not large enough, or the business rules not that many in order to make sense. I think a simple 3-layer approach would have made more sense, due to the thinness of the domain in this situation.

- My first implementation was making use of the Customsearch API from Google, but the secrets management and the complexity of the implementation didn't seem suitable for the complexity of the task. I read that Google might ban your IP if you scrap their website, but due to the nature of the interface of this software; I don't think that will be the case, the user moves to slow for Google to consider it a bot.

- I am not happy with the algorithm that I developed in the `SearchJavascriptLibrary` implementation. I can't identify used libraries when they are uglified or bundled, and I just considered them as part of webpack when they use some of the conventions set in the Webpack config. However, there is not guarantee that these files are part of a webpack bundle and it is a big assumption from my side.<br/> After some investigation I think the most reliable way of identifying the javascript libraries used inside a js file would to call a node.js file from Java (https://www.npmjs.com/package/js-library-detector) or consume an API like this one: https://www.wappalyzer.com

### Decisions I took along the way

- I decided to keep a track of multiple searches in memory. My assumption was that a possible user would like to know the most used JS libraries in the whole web, instead of just in one search query. I opted to save everything on a HashMap for simplicity's sake, but it could easily be replaced by Mongo or something a little more robuts (quite an overstatement to consider a HashMap a robust storage solution).

- I decided to just parse the library names found in the `<script>` tags, without taking into consideration the bundled libraries. There are just 2 options that I could have found feasible on how to retrieve the libraries used in such cases. The first one (and doable) is to read all the global variable names saved in the `window` object, which is only accessible via JS API. The other one is to reverse engineer those files, and I think it defeats the purpose of the task. <br/> However, to display some nicer results I created a set of the most commonly used JS frameworks, in order to count them more efficiently. My algorithm cannot judge if the file has been loaded twice during the same network call, which is something I would have liked to improve. 

- I saw that a lot of results in a Google Search are actually Google links. I decided to filter all google URLs from the scrapping algorithm for performance sake, although I know it could not be accurate if the user tries to scrap a google result from a google search term. 

### How to build it

```mvn clean package```

### How to run it

```java -jar target/scalable-capital-web-crawler-1.0-SNAPSHOT.jar```
 