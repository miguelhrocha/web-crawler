package com.scalablecapital;

import com.scalablecapital.dataproviders.network.GoogleJSoupSearchGateway;
import com.scalablecapital.dataproviders.network.JSoupSupplier;
import com.scalablecapital.dataproviders.network.SearchGateway;
import com.scalablecapital.domain.model.LibraryRanking;
import com.scalablecapital.domain.usecases.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.Collection;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Main {

    // If I were developing a more robust solution, I would use a DI framework, that's why I am always using the interface as the type instead of the concrete one. I am mostly experienced with Spring and Guice, but Dagger should also do the job.
    private static final Executor executor = Executors.newCachedThreadPool();
    private static final JSoupSupplier jsoupSupplier = new JSoupSupplier() {
    };
    private static final SearchGateway searchGateway = new GoogleJSoupSearchGateway("https://google.com", jsoupSupplier, executor);
    private static final SearchLibrary searchLibrary = new SearchJavascriptLibrary(jsoupSupplier, executor);
    private static final RankMostUsedLibraries libraryRankingCalculator = new RankMostUsedLibrariesInMemory();

    public static void main(String[] args) {
        final long totalExecutionStartTime = System.nanoTime();
        try (var scanner = new Scanner(System.in)) {
            while (true) {
                System.out.print("Search: ");
                final String searchTerm = Jsoup.clean(scanner.next(), Whitelist.basic());
                System.out.println(String.format("Searching for: %s", searchTerm));

                final long searchStartTime = System.nanoTime();
                searchFor(searchTerm);
                final long searchEndTime = System.nanoTime();
                System.out.println(String.format("Search for term %s execution time: %s milliseconds", searchTerm, TimeUnit.MILLISECONDS.convert(searchEndTime - searchStartTime, TimeUnit.NANOSECONDS)));

                System.out.println();
                System.out.print("Do you want to search another term? (Y/N): ");
                final String continueFlag = scanner.next();
                System.out.println();

                if (!continueFlag.equalsIgnoreCase("Y")) {
                    final long totalExecutionEndTime = System.nanoTime();
                    System.out.println(String.format("Total execution time: %s milliseconds", TimeUnit.MILLISECONDS.convert(totalExecutionEndTime - totalExecutionStartTime, TimeUnit.NANOSECONDS)));
                    System.exit(1);
                }
            }
        }
    }

    private static void searchFor(String line) {
        System.out.println("Scrapping Google results");
        final var results = searchGateway.searchWebsitesFor(line);
        System.out.println(results);

        System.out.println("Fetching libraries from websites.");
        final var scripts = searchLibrary.extractLibrariesFromWebsites(results);
        System.out.println(scripts);

        final var ranking = libraryRankingCalculator.calculateRanking(scripts);
        System.out.println(String.format("Ranking:\n%s", parseReadableRanking(ranking)));
    }

    private static String parseReadableRanking(final Collection<LibraryRanking> libraryRankings) {
        return libraryRankings.stream()
                .map(LibraryRanking::toString)
                .collect(Collectors.joining("\n"));
    }

}
