package com.scalablecapital.domain.model;

import java.util.Objects;
import java.util.StringJoiner;

public class LibraryRanking {

    private final String name;
    private final int timesFound;

    public LibraryRanking(String name, int timesFound) {
        this.name = name;
        this.timesFound = timesFound;
    }

    public String getName() {
        return name;
    }

    public int getTimesFound() {
        return timesFound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryRanking that = (LibraryRanking) o;
        return timesFound == that.timesFound &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, timesFound);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ","[", "]")
                .add("name='" + name + "'")
                .add("timesFound=" + timesFound)
                .toString();
    }
}
