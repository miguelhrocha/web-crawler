package com.scalablecapital.domain.usecases;

import com.scalablecapital.domain.model.LibraryRanking;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.*;

public class RankMostUsedLibrariesInMemory implements RankMostUsedLibraries {

    private static final int DEFAULT_RANK_LIMIT = 5;
    // I would have probably abstract this to a repository
    private final Map<String, Long> inMemoryRankingStorage = new HashMap<>();

    @Override
    public Collection<LibraryRanking> calculateRanking(Collection<String> unorderedLibraries) {
        final var unorderedLibrariesRanking = unorderedLibraries.stream()
                .collect(groupingBy(library -> library, counting()));
         unorderedLibrariesRanking.forEach((name, occurrence) -> inMemoryRankingStorage.merge(name, occurrence, Long::sum));

        return inMemoryRankingStorage
                .entrySet()
                .stream()
                .sorted((Map.Entry.comparingByValue(reverseOrder())))
                .limit(DEFAULT_RANK_LIMIT)
                .map(libraryNameOccurrenceEntry -> new LibraryRanking(libraryNameOccurrenceEntry.getKey(), Math.toIntExact(libraryNameOccurrenceEntry.getValue())))
                .collect(Collectors.toUnmodifiableList());
    }

}
