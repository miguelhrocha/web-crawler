package com.scalablecapital.domain.usecases;

import com.scalablecapital.domain.model.LibraryRanking;

import java.util.Collection;

public interface RankMostUsedLibraries {

    Collection<LibraryRanking> calculateRanking(Collection<String> unorderedLibraries);

}
