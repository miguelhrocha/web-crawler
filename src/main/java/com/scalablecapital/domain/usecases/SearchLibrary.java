package com.scalablecapital.domain.usecases;

import java.util.Collection;

public interface SearchLibrary {

    Collection<String> extractLibrariesFromWebsites(final Collection<String> websiteUrls);

}
