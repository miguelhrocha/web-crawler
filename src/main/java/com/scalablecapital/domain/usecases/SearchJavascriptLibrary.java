package com.scalablecapital.domain.usecases;

import com.scalablecapital.dataproviders.network.JSoupSupplier;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SearchJavascriptLibrary implements SearchLibrary {

    private final static Set<String> COMMON_JS_LIBRARY = Collections.unmodifiableSet(Set.of(
            "bootstrap", "react", "angular", "jquery", "redux", "vue", "backbone", "ember", "meteor", "polymer", "os", "d3", "parsley", "polyfill"
    ));
    // This might be a wrong assumption, I am relying too much in Webpack conventions:
    private final static Set<String> WEBPACK_FILES = Collections.unmodifiableSet(Set.of("vendor", "main"));
    private final static Pattern LIBRARY_NAME_PATTERN = Pattern.compile("[[A-Za-z]-]+?(?=\\.min.js|\\.js|-)");
    private final JSoupSupplier jsoupSupplier;
    private final Executor executor;

    public SearchJavascriptLibrary(JSoupSupplier jsoupSupplier, Executor executor) {
        this.jsoupSupplier = jsoupSupplier;
        this.executor = executor;
    }

    @Override
    public Collection<String> extractLibrariesFromWebsites(final Collection<String> websiteUrls) {
        final var requests = websiteUrls.stream()
                .map(url -> CompletableFuture.supplyAsync(jsoupSupplier.getDocument(url), executor)
                        .thenApplyAsync(parseJavascriptLibraryFromScriptTag()));

        return requests.flatMap(CompletableFuture::join)
                .collect(Collectors.toUnmodifiableList());
    }

    private Function<Document, Stream<String>> parseJavascriptLibraryFromScriptTag() {
        return document -> document.select("script[src]")
                .stream()
                .filter(element -> element.attr("src").endsWith(".js"))
                .map(this::mapToLibraryResult);
    }

    private String mapToLibraryResult(Element element) {
        final var srcAttribute = element.attr("src");
        final var jsLibrary = srcAttribute.contains("/") ? srcAttribute.substring(srcAttribute.lastIndexOf("/")).toLowerCase() : srcAttribute;
        return parseLibraryName(jsLibrary);
    }

    private String parseLibraryName(String jsLibrary) {
        final var commonLibraryFound = COMMON_JS_LIBRARY.stream()
                .filter(jsLibrary::contains)
                .findAny();
        if (commonLibraryFound.isPresent()) {
            return commonLibraryFound.get();
        } else if (WEBPACK_FILES.stream().anyMatch(jsLibrary::contains)) {
            return "webpack";
        } else {
            final var libraryNameMatcher = LIBRARY_NAME_PATTERN.matcher(jsLibrary);
            return libraryNameMatcher.find() ? libraryNameMatcher.group() : jsLibrary;
        }
    }

}
