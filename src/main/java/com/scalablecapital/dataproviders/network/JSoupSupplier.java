package com.scalablecapital.dataproviders.network;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.function.Supplier;

public interface JSoupSupplier {

    default Supplier<Document> getDocument(String url) {
        return () -> {
            try {
                return Jsoup.connect(url).get();
            } catch (IOException e) {
                return Document.createShell("ERROR");
            }
        };
    }

}
