package com.scalablecapital.dataproviders.network;

import org.jsoup.nodes.Document;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GoogleJSoupSearchGateway implements SearchGateway {

    private final String googleSearchQuery;
    private final JSoupSupplier jsoupSupplier;
    private final Executor executor;

    public GoogleJSoupSearchGateway(String googleHost, JSoupSupplier jsoupSupplier, Executor executor) {
        this.googleSearchQuery = googleHost + "/search?hl=en&num=10&q=%s";
        this.jsoupSupplier = jsoupSupplier;
        this.executor = executor;
    }

    @Override
    public Collection<String> searchWebsitesFor(String searchTerm) {
        final var searchQuery = String.format(googleSearchQuery, searchTerm);
        final var searchRequest = CompletableFuture.supplyAsync(jsoupSupplier.getDocument(searchQuery), executor)
                .thenApplyAsync(parseGoogleSearchResults());

        return searchRequest
                .join()
                .collect(Collectors.toUnmodifiableList());
    }

    private Function<Document, Stream<String>> parseGoogleSearchResults() {
        return document -> document.select("a")
                .stream()
                .map(element -> element.attr("href"))
                .filter(url -> url.startsWith("https") && !url.contains("google"));
    }
}
