package com.scalablecapital.dataproviders.network;

import java.util.Collection;

public interface SearchGateway {

    Collection<String> searchWebsitesFor(String searchTerm);

}
