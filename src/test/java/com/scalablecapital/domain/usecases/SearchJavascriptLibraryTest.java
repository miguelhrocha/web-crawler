package com.scalablecapital.domain.usecases;

import com.scalablecapital.dataproviders.network.JSoupSupplier;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SearchJavascriptLibraryTest {

    private static final String GIVEN_WEBSITE = "https://www.wikipedia.org";
    @Spy
    private final Executor executor = Executors.newFixedThreadPool(2);
    @Mock
    private JSoupSupplier jsoupSupplier;
    @InjectMocks
    private SearchJavascriptLibrary victim;

    @Test
    public void shouldBeAbleToFindJavascriptFilesInScriptTagsInAGivenDOMTree() {
        // I really don't like to mock objects, when stubbing is needed
        final var mockedDocument = givenMockedJSoupRequestOfDOM("/someLibrary.js");
        final var expectedLibrariesFound = List.of("somelibrary");
        when(jsoupSupplier.getDocument(GIVEN_WEBSITE)).thenReturn(() -> mockedDocument);

        final var actualLibrariesFound = victim.extractLibrariesFromWebsites(List.of(GIVEN_WEBSITE));

        assertEquals(expectedLibrariesFound, actualLibrariesFound);
    }

    @Test
    public void shouldReturnEmptyCollectionInCaseAllCrawlingRequestsReturnErrors() {
        final var nullDocument = Document.createShell("ERROR");
        when(jsoupSupplier.getDocument(GIVEN_WEBSITE)).thenReturn(() -> nullDocument);

        final var actualLibrariesFound = victim.extractLibrariesFromWebsites(List.of(GIVEN_WEBSITE));

        assertTrue(actualLibrariesFound.isEmpty());
    }

    @ParameterizedTest
    @CsvSource({"bootstrap/3.3.7/js/bootstrap.min.js,bootstrap", "/jquery-1.12.4.min.js,jquery", "katex.min.js,katex"})
    public void shouldMapLibraryResultWithJustTheLibraryName(String givenLibrary, String expectedName) {
        final var mockedDocument = givenMockedJSoupRequestOfDOM(givenLibrary);
        when(jsoupSupplier.getDocument(GIVEN_WEBSITE)).thenReturn(() -> mockedDocument);

        final var actualLibrariesFound = victim.extractLibrariesFromWebsites(List.of(GIVEN_WEBSITE));
        final var actualName = actualLibrariesFound.iterator().next();

        assertEquals(expectedName, actualName);
    }

    @Test
    public void shouldCreatedUsedLibraryFromCommonlyUsedJsLibrariesWhenTheseAreAvailableInTheScriptObject() {
        final var givenLibrary = "/environment-bootstrap-5e6827ef.js";
        final var mockedDocument = givenMockedJSoupRequestOfDOM(givenLibrary);
        final var expectedName = "bootstrap";
        when(jsoupSupplier.getDocument(GIVEN_WEBSITE)).thenReturn(() -> mockedDocument);

        final var actualLibrariesFound = victim.extractLibrariesFromWebsites(List.of(GIVEN_WEBSITE));
        final var actualName = actualLibrariesFound.iterator().next();

        assertEquals(expectedName, actualName);
    }

    @Test
    public void shouldReturnWebpackLibraryWhenJSFileHasVendorNameInIt() {
        final var givenLibrary = "/vendor-5e6827ef.js";
        final var mockedDocument = givenMockedJSoupRequestOfDOM(givenLibrary);
        final var expectedName = "webpack";
        when(jsoupSupplier.getDocument(GIVEN_WEBSITE)).thenReturn(() -> mockedDocument);

        final var actualLibrariesFound = victim.extractLibrariesFromWebsites(List.of(GIVEN_WEBSITE));
        final var actualName = actualLibrariesFound.iterator().next();

        assertEquals(expectedName, actualName);
    }

    private Document givenMockedJSoupRequestOfDOM(String givenLibrary) {
        final var mockedDocument = mock(Document.class);
        final var scriptAttributes = new Attributes();
        scriptAttributes.put("src", givenLibrary);
        final var givenElement = new Element(Tag.valueOf("script"), null, scriptAttributes);
        when(mockedDocument.select("script[src]")).thenReturn(new Elements(givenElement));
        return mockedDocument;
    }

}