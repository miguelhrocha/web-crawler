package com.scalablecapital.domain.usecases;

import com.scalablecapital.domain.model.LibraryRanking;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RankMostUsedLibrariesInMemoryTest {

    private final RankMostUsedLibrariesInMemory victim = new RankMostUsedLibrariesInMemory();

    @Test
    public void shouldCalculateMostUsedLibrariesByLibraryName() {
        final var mostUsedLibrary = "jquery";
        final var mostUsedLibrarySecondOccurrence = "jquery";
        final var secondPlaceUsedLibrary = "react";
        final var expectedRanking = List.of(new LibraryRanking("jquery", 2), new LibraryRanking("react", 1));

        final var actualRanking = victim.calculateRanking(List.of(mostUsedLibrary, mostUsedLibrarySecondOccurrence, secondPlaceUsedLibrary));

        assertEquals(expectedRanking, actualRanking);
    }

    @Test
    public void shouldCalculateMostUsedLibrariesTakingIntoConsiderationPreviousExecutionRanking() {
        final var mostUsedLibraryFirstExecution = "jquery";
        victim.calculateRanking(Set.of(mostUsedLibraryFirstExecution));
        final var mostUsedLibrarySecondExecution = "jquery";
        final var expectedRanking = List.of(new LibraryRanking("jquery", 2));

        final var actualRanking = victim.calculateRanking(Set.of(mostUsedLibrarySecondExecution));

        assertEquals(expectedRanking, actualRanking);
    }

    @Test
    public void shouldReturnJustTheTop5MostUsedLibraries() {
        final var actualRanking = victim.calculateRanking(Set.of("jquery", "react", "redux", "ember", "webpack", "bootstrap"));

        assertEquals(5, actualRanking.size());
    }

}