package com.scalablecapital.dataproviders;

import com.scalablecapital.dataproviders.network.GoogleJSoupSearchGateway;
import com.scalablecapital.dataproviders.network.JSoupSupplier;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GoogleJSoupSearchGatewayTest {

    private static final String GOOGLE_HOST = "http://localhost";
    private static final Executor executor = Executors.newFixedThreadPool(2);
    @Mock
    private JSoupSupplier jSoupSupplier;

    private GoogleJSoupSearchGateway victim;

    @BeforeEach
    public void setUp() {
        victim = new GoogleJSoupSearchGateway(GOOGLE_HOST, jSoupSupplier, executor);
    }

    @Test
    public void shouldBeAbleToRetrieveUrlsFromGoogleResultPage() {
        final var givenSearchTerm = "wikipedia";
        final var givenSearchQuery = "http://localhost/search?hl=en&num=10&q=wikipedia";
        final var givenDocument = givenMockedJSoupGoogleResponse("https://wikipedia.org");
        final var expectedResponse = List.of("https://wikipedia.org");
        when(jSoupSupplier.getDocument(givenSearchQuery)).thenReturn(() -> givenDocument);

        final var actualResponse = victim.searchWebsitesFor(givenSearchTerm);

        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void shouldJustRetrieveElementsWithHrefProperty() {
        final var givenSearchTerm = "wikipedia";
        final var givenSearchQuery = "http://localhost/search?hl=en&num=10&q=wikipedia";
        final var mockedDocument = mock(Document.class);
        when(mockedDocument.select("a")).thenReturn(new Elements());
        when(jSoupSupplier.getDocument(givenSearchQuery)).thenReturn(() -> mockedDocument);

        final var actualResponse = victim.searchWebsitesFor(givenSearchTerm);

        assertTrue(actualResponse.isEmpty());
    }

    @Test
    public void shouldFilterGoogleUrlsFromSearchToImprovePerformance() {
        final var givenSearchTerm = "google";
        final var givenSearchQuery = "http://localhost/search?hl=en&num=10&q=google";
        final var givenDocument = givenMockedJSoupGoogleResponse("https://google.com");
        when(jSoupSupplier.getDocument(givenSearchQuery)).thenReturn(() -> givenDocument);

        final var actualResponse = victim.searchWebsitesFor(givenSearchTerm);

        assertTrue(actualResponse.isEmpty());
    }

    private Document givenMockedJSoupGoogleResponse(String givenUrl) {
        final var mockedDocument = mock(Document.class);
        final var scriptAttributes = new Attributes();
        scriptAttributes.put("href", givenUrl);
        final var givenElement = new Element(Tag.valueOf("a"), null, scriptAttributes);
        when(mockedDocument.select("a")).thenReturn(new Elements(givenElement));
        return mockedDocument;
    }
}